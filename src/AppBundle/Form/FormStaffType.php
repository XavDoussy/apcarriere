<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\DateTime;

class FormStaffType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('salFix', IntegerType::class, array(
                'required' => false
            ))
            ->add('primeJ', IntegerType::class, array(
                'required' => false
            ))
            ->add('joursT', IntegerType::class, array(
                'required' => false
            ))
            ->add('varE', IntegerType::class, array(
                'required' => false
            ))
            ->add('varP', IntegerType::class, array(
                'required' => false
            ))
            ->add('coopt', IntegerType::class, array(
                'required' => false
            ))
            ->add('appAff', IntegerType::class, array(
                'required' => false
            ))
            ->add('autres', IntegerType::class, array(
                'required' => false
            ))
            ->add('hSupp', IntegerType::class, array(
                'required' => false
            ))
            ->add('astreintes', IntegerType::class, array(
                'required' => false
            ))
            ->add('participation', IntegerType::class, array(
                'required' => false
            ))
            ->add('position', IntegerType::class, array(
                'required' => false
            ))
            ->add('coef', IntegerType::class, array(
                'required' => false
            ))
            ->add('qualif', TextType::class, array(
                'required' => false
            ))
            ->add('statut', TextType::class, array(
                'required' => false
            ))
            ->add('tauxH', IntegerType::class, array(
                'required' => false
            ))
            ->add('valeurP', IntegerType::class, array(
                'required' => false
            ))
            ->add('minima', IntegerType::class, array(
                'required' => false
            ))
            ->add('date', DateType::class, array(
                'required' => false
            ))
            ->add('cout' , IntegerType::class, array(
                'required' => false
            ))
            ->add('nbJours', IntegerType::class, array(
                'required' => false
            ))
            ->add('initiative', TextType::class, array(
                'required' => false
            ))
            ->add('contenu', TextType::class, array(
                'required' => false
            ))
            ->add('mode', TextType::class, array(
                'required' => false
            ))
            ->add('contrepartie', TextType::class, array(
                'required' => false
            ))
            ->add('ancPost', ChoiceType::class, array(
              'required' => false,
              'attr' => array(
                  'class' => 'rating'),
                'choices'=>array(
                '1'=>1,
                '2'=>2,
                '3'=>3,
                '4'=>4,
                '5'=>5,
            )))
            ->add('ancPostCom', TextType::class, array(
                'required' => false
            ))
            ->add('compTech', ChoiceType::class, array(
              'required' => false,
              'attr' => array(
                  'class' => 'rating'),
                'choices'=>array(
                '1'=>1,
                '2'=>2,
                '3'=>3,
                '4'=>4,
                '5'=>5,
            )))
            ->add('compTechCom', TextType::class, array(
                'required' => false
            ))
            ->add('mobil', ChoiceType::class, array(
              'required' => false,
              'attr' => array(
                  'class' => 'rating'),
                'choices'=>array(
                '1'=>1,
                '2'=>2,
                '3'=>3,
                '4'=>4,
                '5'=>5,
            )))
            ->add('mobilCom', TextType::class, array(
                'required' => false
            ))
            ->add('participA', ChoiceType::class, array(
              'required' => false,
              'attr' => array(
                  'class' => 'rating'),
                'choices'=>array(
                '1'=>1,
                '2'=>2,
                '3'=>3,
                '4'=>4,
                '5'=>5,
            )))
            ->add('participACom', TextType::class, array(
                'required' => false
            ))
            ->add('motivImpli', ChoiceType::class, array(
              'required' => false,
              'attr' => array(
                  'class' => 'rating'),
                'choices'=>array(
                '1'=>1,
                '2'=>2,
                '3'=>3,
                '4'=>4,
                '5'=>5,
            )))
            ->add('motivImpliCom', TextType::class, array(
                'required' => false
            ))
            ->add('adhesA', ChoiceType::class, array(
              'required' => false,
              'attr' => array(
                  'class' => 'rating'),
                'choices'=>array(
                '1'=>1,
                '2'=>2,
                '3'=>3,
                '4'=>4,
                '5'=>5,
            )))
            ->add('adhesACom', TextType::class, array(
                'required' => false
            ))
            ->add('comments', TextareaType::class, array(
                'required' => false
            ))
            ->add('objectifs', TextareaType::class, array(
                'required' => false
            ))
            ->add('save', SubmitType::class, array(
                'label' => 'Sauvegarder'
            ));
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\FormStaff'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_formstaff';
    }


}
