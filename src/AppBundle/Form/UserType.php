<?php
/**
 * Created by PhpStorm.
 * User: apside
 * Date: 11/08/2017
 * Time: 15:26
 */

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use FOS\UserBundle\Util\LegacyFormHelper;

class UserType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
      $builder
      ->add('nom' , TextType::class)
      ->add('prenom' , TextType::class)
      ->add('username', TextType::class, array(
        'label' => 'Pseudo'
      ))
      ->add('email', TextType::class)
      ->add('mailPerso', TextType::class)
      ->add('agence' , ChoiceType::class, array(
      'choices'  => array(
          'Orleans' => "Orléans",
          'Tours' => "Tours",)))
      ->add('startdate', DateType::class, array(
        'data' => new \DateTime('now'),
        'years' => range(date('Y') -43, date('Y')),
        'format' => 'dd-MM-yyyy',
        'label' => 'Date d\'embauche',
))
      ->add('plainPassword', LegacyFormHelper::getType('Symfony\Component\Form\Extension\Core\Type\RepeatedType'), array(
  'type' => LegacyFormHelper::getType('Symfony\Component\Form\Extension\Core\Type\PasswordType'),
  'options' => array('translation_domain' => 'FOSUserBundle'),
  'first_options' => array('label' => 'Mot de passe'),
  'second_options' => array('label' => 'Confirmer mot de passe'),
  'invalid_message' => 'fos_user.password.mismatch',
))
          ;

    }


    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\User'
        ));
    }

}
