<?php
/**
 * Created by PhpStorm.
 * User: apside
 * Date: 11/08/2017
 * Time: 15:26
 */

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SurveyType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('surveydate', DateType::class, array(
                'data' => new \DateTime('now')))
            ->add('dureemission', ChoiceType::class, array(
              'required' => false,
              'attr' => array(
                  'class' => 'rating'),
                'choices'=>array(
                '1'=>1,
                '2'=>2,
                '3'=>3,
                '4'=>4,
                '5'=>5,
            )))
            ->add('dureemissioncom', TextType::class, array(
                'required' => false
            ))
            ->add('carriereSouhait', ChoiceType::class, array(
              'required' => false,
              'attr' => array(
                  'class' => 'rating'),
                'choices'=>array(
                '1'=>1,
                '2'=>2,
                '3'=>3,
                '4'=>4,
                '5'=>5,
            )))
            ->add('carriereSouhaitcom', TextType::class, array(
                'required' => false
            ))
            ->add('equipeClient', ChoiceType::class, array(
              'required' => false,
              'attr' => array(
                  'class' => 'rating'),
                'choices'=>array(
                '1'=>1,
                '2'=>2,
                '3'=>3,
                '4'=>4,
                '5'=>5,
            )))
            ->add('equipeClientcom', TextType::class, array(
                'required' => false
            ))
            ->add('chefprojet', ChoiceType::class, array(
              'required' => false,
              'attr' => array(
                  'class' => 'rating'),
                'choices'=>array(
                '1'=>1,
                '2'=>2,
                '3'=>3,
                '4'=>4,
                '5'=>5,
            )))
            ->add('chefprojetcom', TextType::class, array(
                'required' => false
            ))
            ->add('intTech', ChoiceType::class, array(
              'required' => false,
              'attr' => array(
                  'class' => 'rating'),
                'choices'=>array(
                '1'=>1,
                '2'=>2,
                '3'=>3,
                '4'=>4,
                '5'=>5,
            )))
            ->add('intTechcom', TextType::class, array(
                'required' => false
            ))
            ->add('intFonct', ChoiceType::class, array(
              'required' => false,
              'attr' => array(
                  'class' => 'rating'),
                'choices'=>array(
                '1'=>1,
                '2'=>2,
                '3'=>3,
                '4'=>4,
                '5'=>5,
            )))
            ->add('intFonctcom', TextType::class, array(
                'required' => false
            ))
            ->add('siteGeo', ChoiceType::class, array(
              'required' => false,
              'attr' => array(
                  'class' => 'rating'),
                'choices'=>array(
                '1'=>1,
                '2'=>2,
                '3'=>3,
                '4'=>4,
                '5'=>5,
            )))
            ->add('siteGeocom', TextType::class, array(
                'required' => false
            ))
            ->add('deplHorair', ChoiceType::class, array(
              'required' => false,
              'attr' => array(
                  'class' => 'rating'),
                'choices'=>array(
                '1'=>1,
                '2'=>2,
                '3'=>3,
                '4'=>4,
                '5'=>5,
            )))
            ->add('deplHoraircom', TextType::class, array(
                'required' => false
            ))
            ->add('globalmi', ChoiceType::class, array(
              'required' => false,
              'attr' => array(
                  'class' => 'rating'),
                'choices'=>array(
                '1'=>1,
                '2'=>2,
                '3'=>3,
                '4'=>4,
                '5'=>5,
            )))
            ->add('globalmicom', TextType::class, array(
                'required' => false
            ))
            ->add('evolutionLast', TextareaType::class, array(
                'required' => false
            ))
            ->add('commentsmi', TextareaType::class, array(
                'required' => false
            ))
            ->add('relStructure', ChoiceType::class, array(
              'required' => false,
              'attr' => array(
                  'class' => 'rating'),
                'choices'=>array(
                '1'=>1,
                '2'=>2,
                '3'=>3,
                '4'=>4,
                '5'=>5,
            )))
            ->add('relStructurecom', TextType::class, array(
                'required' => false
            ))
            ->add('ecouteBesoin', ChoiceType::class, array(
              'required' => false,
              'attr' => array(
                  'class' => 'rating'),
                'choices'=>array(
                '1'=>1,
                '2'=>2,
                '3'=>3,
                '4'=>4,
                '5'=>5,
            )))
            ->add('ecouteBesoincom', TextType::class, array(
                'required' => false
            ))
            ->add('strategieDev', ChoiceType::class, array(
              'required' => false,
              'attr' => array(
                  'class' => 'rating'),
                'choices'=>array(
                '1'=>1,
                '2'=>2,
                '3'=>3,
                '4'=>4,
                '5'=>5,
            )))
            ->add('strategieDevcom', TextType::class, array(
                'required' => false
            ))
            ->add('politicSalarial', ChoiceType::class, array(
              'required' => false,
              'attr' => array(
                  'class' => 'rating'),
                'choices'=>array(
                '1'=>1,
                '2'=>2,
                '3'=>3,
                '4'=>4,
                '5'=>5,
            )))
            ->add('politicSalarialcom', TextType::class, array(
                'required' => false
            ))
            ->add('espritEnt', ChoiceType::class, array(
              'required' => false,
              'attr' => array(
                  'class' => 'rating'),
                'choices'=>array(
                '1'=>1,
                '2'=>2,
                '3'=>3,
                '4'=>4,
                '5'=>5,
            )))
            ->add('espritEntcom', TextType::class, array(
                'required' => false
            ))
            ->add('transparence', ChoiceType::class, array(
              'required' => false,
              'attr' => array(
                  'class' => 'rating'),
                'choices'=>array(
                '1'=>1,
                '2'=>2,
                '3'=>3,
                '4'=>4,
                '5'=>5,
            )))
            ->add('transparencecom', TextType::class, array(
                'required' => false
            ))
            ->add('politicInfo', ChoiceType::class, array(
              'required' => false,
              'attr' => array(
                  'class' => 'rating'),
                'choices'=>array(
                '1'=>1,
                '2'=>2,
                '3'=>3,
                '4'=>4,
                '5'=>5,
            )))
            ->add('politicInfocom', TextType::class, array(
                'required' => false
            ))
            ->add('globalap', ChoiceType::class, array(
              'required' => false,
              'attr' => array(
                  'class' => 'rating'),
                'choices'=>array(
                '1'=>1,
                '2'=>2,
                '3'=>3,
                '4'=>4,
                '5'=>5,
            )))
            ->add('globalapcom', TextType::class, array(
                'required' => false
            ))
            ->add('commentsap', TextareaType::class, array(
                'required' => false
            ))
            ->add('save', SubmitType::class);
    }
    public function configureOptions(OptionsResolver $resolver){
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Survey'
        ));
    }
}
