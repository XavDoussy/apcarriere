<?php
/**
 * Created by PhpStorm.
 * User: apside
 * Date: 09/08/2017
 * Time: 14:00
 */

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\DateTime;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class RegistrationType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
        ->add('startdate', DateType::class)
        ->add('nom' , TextType::class)
        ->add('prenom' , TextType::class)
        ->add('agence' , ChoiceType::class, array(
        'choices'  => array(
            'Orleans' => "Orléans",
            'Tours' => "Tours",)
    ));
    }

    public function getParent()
    {
        return 'FOS\UserBundle\Form\Type\RegistrationFormType';
    }

    public function getBlockPrefix()
    {
        return 'app_user_registration';
    }
}
