<?php
/**
 * Created by PhpStorm.
 * User: apside
 * Date: 21/08/2017
 * Time: 15:21
 */

namespace AppBundle\Services;

/**
 * Class IntervalService
 * @package AppBundle\Services
 */
class IntervalService
{
    public function displayOrNot($collab)
    {
        $now= new \DateTime();
        $nextSurvey= new \DateTime($collab->getDateAnniv()->getNextSurvey()->format('d-m-Y'));
        $interval = $now->diff($nextSurvey)->days;

        if($interval >= 0 && $interval <= 30){
            return true;
        }

    }
}
