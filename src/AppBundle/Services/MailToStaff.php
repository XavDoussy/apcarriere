<?php
/**
 * Created by PhpStorm.
 * User: apside
 * Date: 29/08/2017
 * Time: 14:04
 */

namespace AppBundle\Services;

use Symfony\Component\DependencyInjection\ContainerInterface;

class MailToStaff
{
    private $templating;

    public function __construct(ContainerInterface $container)
    {
        $this->setTemplating($container->get('templating'));
    }

    /**
     * @return mixed
     */
    public function getTemplating()
    {
        return $this->templating;
    }

    /**
     * @param mixed $templating
     */
    public function setTemplating($templating)
    {
        $this->templating = $templating;
    }

    public function informStaff($selectedMail, \Swift_Mailer $mailer, $request1, $user, $date)
    {
        foreach ($selectedMail as $mailStaff) {
            $message = (new \Swift_Message('Augmentation détectée'))
                ->setFrom('noreply@apside.com')
                ->addBcc($mailStaff)
                ->setBody(
                    $this->getTemplating()->render('Emails/mailingStaff.html.twig', array(
                        'mail' => $mailStaff,
                        'request1' => $request1,
                        'user' => $user,
                        'date' => $date,
                        )),
                    'text/html'
                );

            $mailer->send($message);
        }


    }
}