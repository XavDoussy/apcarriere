<?php
/**
 * Created by PhpStorm.
 * User: apside
 * Date: 21/08/2017
 * Time: 16:22
 */

namespace AppBundle\Services;

/**
 * Class UpdateDateService
 * @package AppBundle\Services
 */
class UpdateDateService
{
    public function changeDateAnniv($collab)
    {
        $date = $collab->getDateAnniv()->getNextSurvey();

        While ($date < new \DateTime('-30 days')) {
            $date->add(new \DateInterval('P6M'));
        }
        return $date;
    }
}