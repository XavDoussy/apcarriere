<?php
/**
 * Created by PhpStorm.
 * User: apside
 * Date: 24/08/2017
 * Time: 14:04
 */

namespace AppBundle\Services;


use Symfony\Component\DependencyInjection\ContainerInterface;

class MailingService
{
    private $templating;

    private $mailer;

    public function __construct(ContainerInterface $container, \Swift_Mailer $mailer)
    {
        $this->mailer = $mailer;
        $this->setTemplating($container->get('templating'));
    }

    /**
     * @return mixed
     */
    public function getTemplating()
    {
        return $this->templating;
    }

    /**
     * @param mixed $templating
     */
    public function setTemplating($templating)
    {
        $this->templating = $templating;
    }

    public function sendMail($listToSends)
    {
        foreach ($listToSends as $listToSend) {

            $message = (new \Swift_Message('Rempli le questionnaire!'))
                ->setFrom('noreply@apside.com')
                ->addBcc($listToSend)
                ->setBody(
                    $this->getTemplating()->render('Emails/mailingRappel.html.twig', array('name' => $listToSend)), 'text/html'
                );

            $this->mailer->send($message);
        }
    }

    public function confirmRegistration($user)
    {
      $userMail = $user->getEmail();

      $message = (new \Swift_Message('Votre compte a été créé!'))
          ->setFrom('noreply@apside.com')
          ->addBcc($userMail)
          ->setBody(
              $this->getTemplating()->render('Emails/registration_email.html.twig', array(
                  'name' => $user->getNom(),
                  'email' => $userMail,
              )),
                  'text/html'
          );

      $this->mailer->send($message);

    }

    public function surveyCompletedMail($user)
    {
        $userAgence = $user->getAgence();
        if($userAgence == 'Orléans'){
            $emailStaff= "lydie@email.com";
        }else{
            $emailStaff= "tours@email.com";
        }

        $message = (new \Swift_Message('L\'enquête a été remplie!'))
            ->setFrom('noreply@apside.com')
            ->addBcc($emailStaff)
            ->setBody(
                $this->getTemplating()->render('Emails/survey_completed_email.html.twig', array(
                    'nom' => $user->getNom(),
                    'prenom' => $user->getPrenom(),
                )),
                    'text/html'
            );

        $this->mailer->send($message);
    }

    public function surveyNotCompletedMail($user, $refusMessage)
    {
        $userAgence = $user->getAgence();
        if($userAgence == 'Orléans'){
            $emailStaff= "lydie@email.com";
        }else{
            $emailStaff= "tours@email.com";
        }

        $message = (new \Swift_Message('Le collaborateur refuse l\'entretien'))
            ->setFrom('noreply@apside.com')
            ->addBcc($emailStaff)
            ->setBody(
                $this->getTemplating()->render('Emails/survey_not_completed_email.html.twig', array(
                    'refusMessage' => $refusMessage,
                    'nom' => $user->getNom(),
                    'prenom' => $user->getPrenom(),
                )),
                    'text/html'
            );

        $this->mailer->send($message);
    }

}
