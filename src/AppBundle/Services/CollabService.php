<?php
/**
 * Created by PhpStorm.
 * User: apside
 * Date: 24/08/2017
 * Time: 14:04
 */

namespace AppBundle\Services;

use AppBundle\Services\MailingService;
use Doctrine\ORM\EntityManagerInterface;

class CollabService
{
    protected $em;

    protected $mailingService;

    public function __construct(MailingService $mailingService, EntityManagerInterface $entityManager)
    {
        $this->em = $entityManager;
        $this->mailingService = $mailingService;
    }

    public function SendMailNotCompletedSurveysAndRetrieveListForReminder()
    {

        $em = $this->em;
        $qb = $this->em->createQueryBuilder();

         $qb->select('d')
            ->from('AppBundle:DateAnniv', 'd');

        $userAnnivs= $qb->getQuery()->getResult();

        $now = new \DateTime();

        $listToCompletes = '';
        foreach ($userAnnivs as $userAnniv) {
            $nextSurvey = $userAnniv->getNextSurvey();
            $interval = $now->diff($nextSurvey)->days;
            if ($interval >= 0 && $interval <= 30) {
                $listToCompletes [] = $userAnniv->getUser();
            }
        }

        $listToSends = '';
        foreach ($userAnnivs as $userAnniv) {
            $user = $userAnniv->getUser();
            dump($user);
            $status = $userAnniv->getStatus();
            $nextSurvey = $userAnniv->getNextSurvey();
            $interval = $now->diff($nextSurvey)->days;
            if ($interval >= 0 && $interval <= 30 && $status == 0 && $user->isEnabled()) {
                $userAnniv->setStatus('1');
                $em->persist($userAnniv);
                $em->flush();
                $listToSends [] = $userAnniv->getUser()->getEmail();
            }
        }

        // Envoie un mail à tous ceux qui sont dans cette liste
        if($listToSends != null){
            $this->mailingService->sendMail($listToSends);
        }
        return $listToCompletes;
    }
}
