<?php
/**
 * Created by PhpStorm.
 * User: apside
 * Date: 24/08/2017
 * Time: 14:04
 */

namespace AppBundle\Services;

use Doctrine\ORM\EntityManagerInterface;

class StatService
{

    protected $em;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->em = $entityManager;
    }

    public function getAllStats()
    {
        $em = $this->em;
        $qb = $this->em->createQueryBuilder();

         $qb->select('s')
            ->from('AppBundle:Survey', 's')
            ->where($qb->expr()->isNotNull('s.dureeMission'));

        $surveys= $qb->getQuery()->getResult();

        $allmission = 0;
        $allapside = 0;
        $countsurvey = count($surveys);
        $satmiperyears = array();
        $satapperyears = array();

        foreach ($surveys as $survey) {

            $date = date_parse($survey->getSurveydate()->format('d-m-Y'));
            $year = $date['year'];
            $survey->setPercentmi();
            $survey->setPercentap();
            $allmission += $survey->getPercentmi();
            $allapside += $survey->getPercentap();
            $satmiperyears[$year][] = $survey->getPercentmi();
            $satapperyears[$year][] = $survey->getPercentap();
        }


        if ($surveys) {
            $allpermi = round($allmission / $countsurvey, 0);
            $allperap = round($allapside / $countsurvey, 0);
            foreach ($satmiperyears as $year => $satmiperyear) {
                $count = 0;
                $total = 0;
                foreach ($satmiperyear as $pourcent) {
                    $total += $pourcent;
                    $count++;
                }
                $avg = $total / $count;
                $totalmiyears [$year] = $avg;

            }

            foreach ($satapperyears as $year => $satapperyear) {
                $count = 0;
                $total = 0;
                foreach ($satapperyear as $pourcent) {
                    $total += $pourcent;
                    $count++;
                }
                $avg = $total / $count;
                $totalapyears [$year] = $avg;
            }

        } else {
            $allpermi = 100;
            $allperap = 100;
            $totalmiyears []= 100;
            $totalapyears []= 100;
        }
        return array('allpermi'=>$allpermi, 'allperap'=>$allperap, 'totalmiyears'=>$totalmiyears, 'totalapyears'=>$totalapyears);

    }
}
