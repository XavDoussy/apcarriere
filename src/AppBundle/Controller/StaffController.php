<?php
/**
 * Created by PhpStorm.
 * User: apside
 * Date: 16/08/2017
 * Time: 15:32
 */

namespace AppBundle\Controller;

use AppBundle\Entity\User;
use AppBundle\Entity\Survey;
use AppBundle\Entity\DateAnniv;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\SearchType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\RegistrationType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use AppBundle\Repository\UserRepository;
use Symfony\Component\HttpFoundation\JsonResponse;
use AppBundle\Services\MailingService;
use AppBundle\Services\CollabService;
use AppBundle\Services\UpdateDateService;
use AppBundle\Services\StatService;

class StaffController extends Controller
{
    /**
     * @Route("/employee", name="employee")
     * @Method({"GET", "POST"})
     */
    public function listAll(Request $request, MailingService $mailingService, CollabService $collabService, StatService $statService)
    {
        $em = $this->getDoctrine()->getManager();

        $recentDates= $em->getRepository('AppBundle:Survey')->lastSurveyDate();
        $surveys = $em->getRepository('AppBundle:Survey')->findAll();
        $userAnnivs = $em->getRepository('AppBundle:DateAnniv')->findAll();
        $formStaffs = $em->getRepository('AppBundle:FormStaff')->findAll();

        $now = new \DateTime();

        $listToCompletes= $collabService->SendMailNotCompletedSurveysAndRetrieveListForReminder();

        $allstats= $statService->getAllStats();
        $allpermi= $allstats['allpermi'];
        $allperap= $allstats['allperap'];
        $totalmiyears= $allstats['totalmiyears'];
        $totalapyears= $allstats['totalapyears'];

          ksort($totalmiyears);
          ksort($totalapyears);

        $query = $this->getDoctrine()->getEntityManager()
            ->createQuery('SELECT u FROM AppBundle:User u WHERE NOT u.roles LIKE :role AND u.enabled = 1')
            ->setParameter('role', '%"ROLE_ADMIN"%');
        $employees = $query->getResult();

        $query2 = $this->getDoctrine()->getEntityManager()
            ->createQuery('SELECT u FROM AppBundle:User u WHERE NOT u.roles LIKE :role AND u.enabled = 0')
            ->setParameter('role', '%"ROLE_ADMIN"%');
        $employeesArchived = $query2->getResult();

        if (isset($_POST['rappel'])) {
            $destinationAdresses []= $_POST['email'];
            $mailingService->sendMail($destinationAdresses);
        }

        if (isset($_POST['hello'])) {
            $iduser= $_POST['id'];
            $user = $em->getRepository('AppBundle:User')->findOneBy(array('id' => $iduser));
            $user->setEnabled(1);
            $em->flush();
        }

        return $this->render('frontstaff/employeeIndex.html.twig', array(
            'employees' => $employees,
            'employeesArchived' => $employeesArchived,
            'recentDates' => $recentDates,
            'surveys' => $surveys,
            'allpermi' => $allpermi,
            'allperap' => $allperap,
            'totalmiyears' => $totalmiyears,
            'totalapyears' => $totalapyears,
            'listToCompletes' => $listToCompletes,
            'userAnnivs' => $userAnnivs,
            'formStaffs' => $formStaffs,
        ));
    }

    /**
     * @param $id
     * @Route("/employee/{id}", name="listsurveys")
     * @return \Symfony\Component\HttpFoundation\Response
     * @Method({"GET", "POST"})
     */
    public function listAllSurveys(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $surveys = $em->getRepository('AppBundle:Survey')
            ->findByUser($id);

        if (isset($_POST['aurevoir'])) {
            $iduser= $_POST['id'];
            $user = $em->getRepository('AppBundle:User')->findOneBy(array('id' => $iduser));
            $user->setEnabled(0);
            $em->flush();
        }

        $user= $em->getRepository('AppBundle:User')
            ->findOneBy(array('id' => $id));

        $form = $this->createForm('AppBundle\Form\RegistrationType', $user);
        $form ->remove('startdate');
        $form ->remove('username');
        $form ->remove('email');
        $form ->remove('plainPassword');
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()){

            $em->persist($user);
            $em->flush();

            return $this->redirectToRoute('employee');
        }

        $evolutionmis[]= 0;
        $evolutionaps[]= 0;
        $countsurvey= count($surveys);
        $totalmi=0;
        $totalap=0;
        foreach ($surveys as $survey) {
            $survey -> setPercentmi();
            $survey -> setPercentap();
            $totalmi+=$survey->getPercentmi();
            $totalap+=$survey->getPercentap();
            $evolutionmis[]= $survey->getPercentmi();
            $evolutionaps[]= $survey->getPercentap();
        }
        if ($surveys) {
            $totalpermi = round($totalmi / $countsurvey, 0);
            $totalperap = round($totalap / $countsurvey, 0);
        } else {
            $totalpermi = 100;
            $totalperap = 100;
        }

        return $this->render('frontstaff/employeeSurveys.html.twig', array(
            'surveys' => $surveys,
            'user' => $user,
            'form' => $form->createView(),
            'evolutionmis'=>$evolutionmis,
            'evolutionaps'=>$evolutionaps,
            'totalpermi'=> $totalpermi,
            'totalperap'=> $totalperap,
        ));
    }

    /**
     * @Route("/new", name="user_new")
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function newAction(Request $request, UpdateDateService $updateDateService, MailingService $mailingService)
    {
        $userManager = $this->get('fos_user.user_manager');
        $user = $userManager->createUser();
        $form = $this->createForm('AppBundle\Form\UserType', $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $em = $this->getDoctrine()->getManager();
            $dateAnniv= new DateAnniv();
            $dateAnniv->setUser($user);
            $dateAnniv->setStatus(0);
            $date= new \DateTime($user->getStartdate()->format('Y-m-d'));
            $date->add(new \DateInterval('P12M'));
            $date->format('Y-m-d');

            While ($date <= new \DateTime('-30 days')) {
                $date->add(new \DateInterval('P6M'));
                }

            $dateAnniv->setNextSurvey($date);
            $em->persist($dateAnniv);
            $em->flush();

                $user->setEnabled(true);
                $user->setRoles(['ROLE_USER']);
                $userManager->updateUser($user);
                $em->persist($user);
                $em->flush();
                $this->addFlash('success', 'Nouveau collab enregistré ');

                $mailingService -> confirmRegistration($user);

                return $this->redirectToRoute('employee');
        }

        return $this->render('frontstaff/new.html.twig', [
            'user' => $user,
            'form' => $form->createView(),
        ]);
    }
}
