<?php

namespace AppBundle\Controller;

use AppBundle\Entity\DateAnniv;
use AppBundle\Entity\Cv;
use AppBundle\Form\CvType;
use AppBundle\Services\IntervalService;
use AppBundle\Services\MailingService;
use AppBundle\Services\UpdateDateService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use AppBundle\Entity\Survey;
use Symfony\Component\Validator\Constraints\DateTime;

class CollabController extends Controller
{

  /**
   * @Route("/collab", name="collab")
   */
    public function indexAction(IntervalService $intervalService, UpdateDateService $updateDateService, Request $request)
    {
      $url = "http://api.giphy.com/v1/gifs/random?api_key=6d68b489e52e4408bc8b4bc82715b682&tag=funny+cat";
      $gif= json_decode(file_get_contents($url));

        $collab = $this->getUser();
        $user = $this->getUser()->getId();
        $em = $this->getDoctrine()->getManager();

        $surveys = $em->getRepository('AppBundle:Survey')
            ->surveysByUserByDate($user);

        $userid= $collab->getId();
        $display = '';
        $lastSurveyDate='';
        $recentDates= $em->getRepository('AppBundle:Survey')->lastSurveyDate();
        foreach($recentDates as $recentDate){
            if($recentDate['user'] == $userid){
                $lastSurveyDate = $recentDate[1];
            }
        }
        $today = new \DateTime();
        if($lastSurveyDate!= null){
        $lastSurvey= new \DateTime($lastSurveyDate);
        }

        if ($intervalService->displayOrNot($collab) == true) {
            $display = 1;
        } else {
            $dateAnniv = $em->getRepository('AppBundle:DateAnniv')
                        ->findOneByUser($user);
            $date = $updateDateService->changeDateAnniv($collab);
            $date = new \DateTime($date->format('Y-m-d'));
            $dateAnniv->setNextSurvey($date);
            $em->persist($dateAnniv);
            $em->flush();
        }

        $evolutionmis[] = 0;
        $evolutionaps[] = 0;
        $countsurvey = count($surveys);
        $totalmi = 0;
        $totalap = 0;
        foreach ($surveys as $survey) {
            $survey->setPercentmi();
            $survey->setPercentap();
            $totalmi += $survey->getPercentmi();
            $totalap += $survey->getPercentap();
            $evolutionmis[] = $survey->getPercentmi();
            $evolutionaps[] = $survey->getPercentap();
        }
        if ($surveys) {
            $totalpermi = round($totalmi / $countsurvey, 0);
            $totalperap = round($totalap / $countsurvey, 0);
        } else {
            $totalpermi = 100;
            $totalperap = 100;
        }

        return $this->render('frontcolab/indexcolab.html.twig', array(
                    'surveys' => $surveys,
                    'evolutionmis' => $evolutionmis,
                    'evolutionaps' => $evolutionaps,
                    'totalpermi' => $totalpermi,
                    'totalperap' => $totalperap,
                    'display' => $display,
                    'gif' => $gif,
                    'form' => $form->createView(),
                ));
    }

}
