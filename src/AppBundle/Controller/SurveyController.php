<?php
/**
 * Created by PhpStorm.
 * User: apside
 * Date: 11/08/2017
 * Time: 15:16
 */

namespace AppBundle\Controller;

use AppBundle\Services\MailingService;
use AppBundle\Entity\DateAnniv;
use AppBundle\Entity\Survey;
use AppBundle\Form\SurveyType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

class SurveyController extends Controller
{
    /**
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route ("/survey", name="survey")
     *
     */
    public function newAction(Request $request, MailingService $mailingService)
    {
        $survey = new Survey();
        $form = $this->createForm(SurveyType::class, $survey);
        $form->remove('surveydate');
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $survey->setSurveydate(new \DateTime('now'));
            $survey->setUser($this->getUser());
            $em->persist($survey);
            $em->flush();
            $em->clear();

//            update de la date d'anniversaire pour déclencher le questionnaire
            $user = $this->getUser();
            $mailingService-> surveyCompletedMail($user);
            $dateAnniv = $em->getRepository('AppBundle:DateAnniv')->findOneByUser($user);
            $date = $user->getDateAnniv()->getNextSurvey();
            $date = new \DateTime($date->format('Y-m-d'));
            $date->add(new \DateInterval('P6M'))->format('Y-m-d');
            $dateAnniv->setNextSurvey($date);
            $dateAnniv->setStatus('0');
            $em->persist($dateAnniv);
            $em->flush();

            return $this->redirectToRoute('homepage');
        }

        return $this->render('survey.html.twig', array(
            'form' => $form->createView(),
        ));
    }

    /**
     * @param Survey $survey
     *
     * @Route ("/survey/{id}", name="detail")
     *
     */
    public function readAction(Survey $survey, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $user= $this->getUser();

        $surveydetail = $em->getRepository('AppBundle:Survey')
                ->findOneBy(array('id'=> $id));
        $userSurvey= $surveydetail->getUser();

        if($user === $userSurvey || $this->isGranted("ROLE_ADMIN")){
            return $this->render('frontcolab/surveydetail.html.twig', array(
                'surveydetail' => $surveydetail,
            ));
        }else{
            throw $this->createAccessDeniedException('You cannot access this page!');
        }

    }

    /**
     * @Route("/refus", name="refus")
     */
      public function noSurveyAction(MailingService $mailingService)
      {
          $em = $this->getDoctrine()->getManager();
          $user= $this->getUser();

          if (isset($_POST['submit'])) {
              $survey = new Survey();
              $survey->setUser($user);
              $survey->setCommentsap($_POST['message']);
              $survey->setSurveydate(new \DateTime('now'));
              $em->persist($survey);
              $em->flush();
              $em->clear();

              $mailingService-> surveyNotCompletedMail($user, $_POST['message']);

              $dateAnniv = $em->getRepository('AppBundle:DateAnniv')->findOneByUser($user);
              $date = $user->getDateAnniv()->getNextSurvey();
              $date = new \DateTime($date->format('Y-m-d'));
              $date->add(new \DateInterval('P6M'))->format('Y-m-d');
              $dateAnniv->setNextSurvey($date);
              $dateAnniv->setStatus('0');
              $em->persist($dateAnniv);
              $em->flush();
          }

          return $this->render('frontcolab/refus.html.twig', array(
             'user' => $user,
          ));

      }

}
