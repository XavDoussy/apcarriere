<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction()
    {
        if ($this->isGranted("ROLE_ADMIN")) {
            return $this->redirectToRoute('employee');

        } elseif ($this->isGranted("ROLE_USER")) {
            return $this->redirectToRoute('collab');

        } else {
            return $this->redirectToRoute('fos_user_security_login');
        }
    }
}
