<?php
/**
 * Created by PhpStorm.
 * User: apside
 * Date: 25/08/2017
 * Time: 10:00
 */

namespace AppBundle\Controller;


use AppBundle\Entity\FormStaff;
use AppBundle\Entity\User;
use AppBundle\Form\FormRaiseType;
use AppBundle\Form\FormStaffType;
use AppBundle\Services\MailToStaff;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;


class FormStaffController extends Controller
{
    /**
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route ("/formStaff/{user}", name="formStaff")
     *
     */
    public function newFormAction(Request $request, User $user)
    {
        $em = $this->getDoctrine()->getManager();

        $formStaffs = $em->getRepository('AppBundle:FormStaff')->findByUser($user);

        $lastMeeting = $em->getRepository('AppBundle:FormStaff')->searchByLastDate($user);

        $formStaff = new FormStaff();
        $form = $this->createForm(FormStaffType::class, $formStaff);
        $form->remove('varP');
        $form->remove('varE');
        $form->handleRequest($request);


        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $formStaff->setUser($user);
            $formStaff->setDateStaff(new \DateTime());
            $salFix = $formStaff->getSalFix();
            $em->persist($formStaff);
            $em->flush();
            if ($lastMeeting) {
                $previousSal = $lastMeeting[0]->getSalFix();
                $diff = $salFix - $previousSal;
            }else{
                $diff = 0;
            }

            if ($diff > 0) {
                $listStaff = $em->getRepository('AppBundle:User')->listAllStaffEmail();
                $mailStaffs = [];
                foreach ($listStaff as $listStaf) {
                    foreach ($listStaf as $listSta) {
                        $mailStaffs [] = $listSta;
                    }
                }
                return $this->render('frontstaff/mailingStaffRaise.html.twig', array(
                    'mailStaffs' => $mailStaffs,
                    'diff' => $diff,
                    'user' => $user,
                ));
            }

            return $this->redirectToRoute('employee');

        }

        return $this->render('frontstaff/formStaffView.html.twig', array(
            'form' => $form->createView(),
            'formStaffs' => $formStaffs,
            'lastMeeting' => $lastMeeting,
            'user' => $user,
        ));
    }

    /**
     *
     * @Route ("/sendMailStaff", name="sendMailStaff")
     */
    public function sendMailStaffAction(Request $request, \Swift_Mailer $mailer, MailToStaff $mailToStaff)
    {

        $request = $request->request->all();
        $request1 = [];
        $mails = [];
        $user = '';
        $date= '';

        foreach ($request as $key => $value) {
            if ($key == 'diff') {
                $request1[] = $value;
            } elseif ($key == 'user') {
                $user = $value;
            } elseif ($key == 'date') {
                $date = $value;
            }else {
                $mails[] = $value;
            }
        }

        $mailToStaff->informStaff($mails, $mailer, $request1, $user, $date);

        return $this->redirectToRoute('employee');
    }

    /**
     * @Route ("/formStaff/update/{id}", name="updateFormStaff")
     * @Method({"GET", "POST"})
     */
    public function updateFormAction(Request $request,FormStaff $formStaff)
    {
        $editForm = $this->createForm('AppBundle\Form\FormStaffType', $formStaff);
        $editForm->remove('varP');
        $editForm->remove('varE');
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('employee');
        }
        return $this->render('frontstaff/formStaffUpdateView.html.twig', array(
            'formStaff' => $formStaff,
            'editForm' => $editForm->createView(),
        ));

    }

}
