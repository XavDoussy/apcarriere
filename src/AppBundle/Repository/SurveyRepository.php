<?php
/**
 * Created by PhpStorm.
 * User: apside
 * Date: 11/08/2017
 * Time: 15:56
 */

namespace AppBundle\Repository;

use AppBundle\Entity\User;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Validator\Constraints\Date;
use AppBundle\Entity\Survey;

class SurveyRepository extends \Doctrine\ORM\EntityRepository
{
    public function lastSurveyDate()
    {
        $query = $this->getEntityManager()->createQuery(
            'SELECT IDENTITY(s.user) as user, MAX (s.surveydate)
                FROM AppBundle:Survey s
                GROUP BY user
                ');
        return $query->getResult();
    }

    public function surveysByUserByDate($user)
    {
        $query = $this->getEntityManager()->createQuery(
            'SELECT s
                FROM AppBundle:Survey s
                WHERE IDENTITY(s.user) = :user AND s.dureeMission IS NOT NULL
                ORDER BY s.surveydate')
        ->setParameter('user', $user);

        return $query->getResult();
    }
}
