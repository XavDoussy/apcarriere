<?php
/**
 * Created by PhpStorm.
 * User: apside
 * Date: 17/08/2017
 * Time: 15:27
 */

namespace AppBundle\Repository;

use AppBundle\Entity\User;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Validator\Constraints\Date;
use AppBundle\Entity\Survey;

class UserRepository extends \Doctrine\ORM\EntityRepository
{
    public function searchByName($input)
    {
        $qb = $this->createQueryBuilder('u')
            ->select('u.username', 'u.roles', 'u.startdate', 'u.id')
            ->where('u.username LIKE :input')
            ->setParameter('input', '%' . $input . '%');    
        return $qb->getQuery()->getResult();
    }

    public function listAllStaffEmail()
    {
        $admin = 'a:1:{i:0;s:10:"ROLE_ADMIN";}';

        $qb2 = $this->createQueryBuilder('u')
            ->select('u.email')
            ->where('u.roles = :admin')
            ->setParameter('admin',  $admin );

        return $qb2->getQuery()->getResult();
    }

}