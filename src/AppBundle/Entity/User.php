<?php
/**
 * Created by PhpStorm.
 * User: apside
 * Date: 07/08/2017
 * Time: 09:23
 */

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use FOS\UserBundle\Model\User as BaseUser;
use Symfony\Component\Validator\Constraints\DateTime;


/**
 * @ORM\Entity
 * @ORM\Table(name="fos_user")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\UserRepository")
 *
 */
class User extends BaseUser
{

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     *
     */
    protected $id;

    /**
     * @ORM\Column(name="nom", type="string")
     */
    protected $nom;

    /**
     * @ORM\Column(name="prenom", type="string")
     */
    protected $prenom;

    /**
     * @ORM\Column(name="mailPerso", type="string", nullable=true)
     */
    protected $mailPerso;

    /**
     * @ORM\Column(name="agence", type="string")
     */
    protected $agence;

    /**
     * @var DateTime
     *
     * @ORM\OneToOne(targetEntity="DateAnniv", mappedBy="user", cascade={"persist", "merge"}, fetch="EAGER")
     */
    protected $dateAnniv;

    /**
     * @var DateTime
     * @ORM\Column(name="startdate", type="datetime")
     *
     */
    protected $startdate;

    /**
     * @var
     * @ORM\OneToMany(targetEntity="FormStaff", mappedBy="user", cascade={"persist", "merge"}, fetch="EAGER")
     */
    public $formStaffs;

    /**
     * @ORM\OneToMany(targetEntity="Survey", mappedBy="user", cascade={"persist", "merge"}, fetch="EAGER")
     */
    public $surveys;

    /**
     * @ORM\OneToMany(targetEntity="Cv", mappedBy="user", cascade={"persist", "merge"}, fetch="EAGER")
     */
    public $cvs;

    /**
     * @return mixed
     */
    public function getCvs()
    {
        return $this->cvs;
    }

    /**
     * @param mixed $cvs
     */
    public function setCvs($cvs)
    {
        $this->cvs = $surveys;
    }


    /**
     * @return mixed
     */
    public function getSurveys()
    {
        return $this->surveys;
    }

    /**
     * @param mixed $surveys
     */
    public function setSurveys($surveys)
    {
        $this->surveys = $surveys;
    }

    /**
     * User constructor.
     * @param int $id
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @return DateTime
     */
    public function getDateAnniv()
    {
        return $this->dateAnniv;
    }

    /**
     * @param DateTime $dateAnniv
     */
    public function setDateAnniv($dateAnniv)
    {
        $this->dateAnniv = $dateAnniv;
    }

    /**
     * @return DateTime
     */
    public function getStartdate()
    {
        return $this->startdate;
    }

    /**
     * @param DateTime $startdate
     */
    public function setStartdate($startdate)
    {
        $this->startdate = $startdate;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    public function getNom()
    {
        return $this->nom;
    }

    public function setNom($nom)
    {
        $this->nom = $nom;
    }

    public function getMailPerso()
    {
        return $this->mailPerso;
    }

    public function setMailPerso($mailPerso)
    {
        $this->mailPerso = $mailPerso;
    }

    public function getPrenom()
    {
        return $this->prenom;
    }

    public function setPrenom($prenom)
    {
        $this->prenom = $prenom;
    }

    public function getAgence()
    {
        return $this->agence;
    }

    public function setAgence($agence)
    {
        $this->agence = $agence;
    }

}
