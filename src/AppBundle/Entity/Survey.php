<?php
/**
 * Created by PhpStorm.
 * User: apside
 * Date: 11/08/2017
 * Time: 15:08
 */

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * MissionSurvey
 * @ORM\Entity
 * @ORM\Table(name="survey")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\SurveyRepository")
 */
class Survey
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="User", inversedBy="surveys", cascade={"persist", "merge"}, fetch="EAGER")
     */
    private $user;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="surveydate", type="datetime")
     */
    private $surveydate;

    /**
     * @var int
     *
     * @ORM\Column(name="duree_mission", type="integer", nullable=true)
     */
    private $dureeMission;

    /**
     * @var string
     *
     * @ORM\Column(name="duree_mission_com", type="string", nullable=true)
     */
    private $dureeMissioncom;

    /**
     * @var int
     *
     * @ORM\Column(name="carriere_souhait", type="integer", nullable=true)
     */
    private $carriereSouhait;

    /**
     * @var string
     *
     * @ORM\Column(name="carriere_souhait_com", type="string", nullable=true)
     */
    private $carriereSouhaitcom;

    /**
     * @var int
     *
     * @ORM\Column(name="equipe_client", type="integer", nullable=true)
     */
    private $equipeClient;

    /**
     * @var string
     *
     * @ORM\Column(name="equipe_client_com", type="string", nullable=true)
     */
    private $equipeClientcom;

    /**
     * @var int
     *
     * @ORM\Column(name="chefprojet", type="integer", nullable=true)
     */
    private $chefprojet;

    /**
     * @var string
     *
     * @ORM\Column(name="chefprojet_com", type="string", nullable=true)
     */
    private $chefprojetcom;

    /**
     * @var int
     *
     * @ORM\Column(name="int_tech", type="integer", nullable=true)
     */
    private $intTech;

    /**
     * @var string
     *
     * @ORM\Column(name="int_tech_com", type="string", nullable=true)
     */
    private $intTechcom;

    /**
     * @var int
     *
     * @ORM\Column(name="int_fonct", type="integer", nullable=true)
     */
    private $intFonct;

    /**
     * @var string
     *
     * @ORM\Column(name="int_fonct_com", type="string", nullable=true)
     */
    private $intFonctcom;

    /**
     * @var int
     *
     * @ORM\Column(name="site_geo", type="integer", nullable=true)
     */
    private $siteGeo;

    /**
     * @var string
     *
     * @ORM\Column(name="site_geo_com", type="string", nullable=true)
     */
    private $siteGeocom;


    /**
     * @var int
     *
     * @ORM\Column(name="depl_horair", type="integer", nullable=true)
     */
    private $deplHorair;

    /**
     * @var string
     *
     * @ORM\Column(name="depl_horair_com", type="string", nullable=true)
     */
    private $deplHoraircom;

    /**
     * @var int
     *
     * @ORM\Column(name="globalmi", type="integer", nullable=true)
     */
    private $globalmi;

    /**
     * @var string
     *
     * @ORM\Column(name="globalmi_com", type="string", nullable=true)
     */
    private $globalmicom;

    /**
     * @var string
     *
     * @ORM\Column(name="evolution_last", type="string", length=255, nullable=true)
     */
    private $evolutionLast;

    /**
     * @var string
     *
     * @ORM\Column(name="commentsmi", type="string", length=255, nullable=true)
     */
    private $commentsmi;

    /**
     * @var int
     *
     * @ORM\Column(name="rel_structure", type="integer", nullable=true)
     */
    private $relStructure;

    /**
     * @var string
     *
     * @ORM\Column(name="rel_structure_com", type="string", nullable=true)
     */
    private $relStructurecom;

    /**
     * @var int
     *
     * @ORM\Column(name="ecoute_besoin", type="integer", nullable=true)
     */
    private $ecouteBesoin;

    /**
     * @var string
     *
     * @ORM\Column(name="ecoute_besoin_com", type="string", nullable=true)
     */
    private $ecouteBesoincom;

    /**
     * @var int
     *
     * @ORM\Column(name="strategie_dev", type="integer", nullable=true)
     */
    private $strategieDev;

    /**
     * @var string
     *
     * @ORM\Column(name="strategie_dev_com", type="string", nullable=true)
     */
    private $strategieDevcom;

    /**
     * @var int
     *
     * @ORM\Column(name="politic_salarial", type="integer", nullable=true)
     */
    private $politicSalarial;

    /**
     * @var string
     *
     * @ORM\Column(name="politic_salarial_com", type="string", nullable=true)
     */
    private $politicSalarialcom;

    /**
     * @var int
     *
     * @ORM\Column(name="esprit_ent", type="integer", nullable=true)
     */
    private $espritEnt;

    /**
     * @var string
     *
     * @ORM\Column(name="esprit_ent_com", type="string", nullable=true)
     */
    private $espritEntcom;

    /**
     * @var int
     *
     * @ORM\Column(name="transparence", type="integer", nullable=true)
     */
    private $transparence;

    /**
     * @var string
     *
     * @ORM\Column(name="transparence_com", type="string", nullable=true)
     */
    private $transparencecom;

    /**
     * @var int
     *
     * @ORM\Column(name="politic_info", type="integer", nullable=true)
     */
    private $politicInfo;

    /**
     * @var string
     *
     * @ORM\Column(name="politic_info_com", type="string", nullable=true)
     */
    private $politicInfocom;

    /**
     * @var int
     *
     * @ORM\Column(name="globalap", type="integer", nullable=true)
     */
    private $globalap;

    /**
     * @var string
     *
     * @ORM\Column(name="globalap_com", type="string", nullable=true)
     */
    private $globalapcom;

    /**
     * @var string
     *
     * @ORM\Column(name="commentsap", type="string", length=255, nullable=true)
     */
    private $commentsap;

    private $percentmi;

    private $percentap;

    /**
     * @return mixed
     */
    public function getPercentmi()
    {
        return $this->percentmi;
    }

    /**
     * @return mixed
     */
    public function getPercentap()
    {
        return $this->percentap;
    }

    public function setPercentmi(){
        $champsRemplis= 0;
        if($this->dureeMission != NULL){
          $champsRemplis++;
        }
        if($this->carriereSouhait != NULL){
          $champsRemplis++;
        }
        if($this->equipeClient != NULL){
          $champsRemplis++;
        }
        if($this->chefprojet != NULL){
          $champsRemplis++;
        }
        if($this->intTech != NULL){
          $champsRemplis++;
        }
        if($this->intFonct != NULL){
          $champsRemplis++;
        }
        if($this->siteGeo != NULL){
          $champsRemplis++;
        }
        if($this->deplHorair != NULL){
          $champsRemplis++;
        }
        if($this->globalmi != NULL){
          $champsRemplis++;
        }
        $division= $champsRemplis*5;
        if($division == 0){
          $division = 1;
        }
        $this->percentmi = round((($this->dureeMission + $this->carriereSouhait + $this->equipeClient + $this->chefprojet +
                    $this->intTech + $this->intFonct + $this->siteGeo + $this->deplHorair + $this->globalmi)*100/$division),0);
    }

    public function setPercentap(){
      $champsRemplis= 0;
      if($this->relStructure != NULL){
        $champsRemplis++;
      }
      if($this->ecouteBesoin != NULL){
        $champsRemplis++;
      }
      if($this->strategieDev != NULL){
        $champsRemplis++;
      }
      if($this->politicSalarial != NULL){
        $champsRemplis++;
      }
      if($this->espritEnt != NULL){
        $champsRemplis++;
      }
      if($this->transparence != NULL){
        $champsRemplis++;
      }
      if($this->politicInfo != NULL){
        $champsRemplis++;
      }
      if($this->globalap != NULL){
        $champsRemplis++;
      }

      $division= $champsRemplis*5;
      if($division == 0){
        $division = 1;
      }
        $this->percentap = round((($this->relStructure + $this->ecouteBesoin + $this->strategieDev + $this->politicSalarial +
                    $this->espritEnt + $this->transparence + $this->politicInfo + $this->globalap)*100/$division),0);
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param User $user
     */
    public function setUser($user)
    {
        $this->user = $user;
    }

    /**
     * @return \DateTime
     */
    public function getSurveydate()
    {
        return $this->surveydate;
    }

    /**
     * @param \DateTime $surveydate
     */
    public function setSurveydate($surveydate)
    {
        $this->surveydate = $surveydate;
    }

    /**
     * @return int
     */
    public function getDureeMission()
    {
        return $this->dureeMission;
    }

    /**
     * @param int $dureeMission
     */
    public function setDureeMission($dureeMission)
    {
        $this->dureeMission = $dureeMission;
    }

    /**
     * @return string
     */
    public function getDureeMissioncom()
    {
        return $this->dureeMissioncom;
    }

    /**
     * @param string $dureeMissioncom
     */
    public function setDureeMissioncom($dureeMissioncom)
    {
        $this->dureeMissioncom = $dureeMissioncom;
    }

    /**
     * @return int
     */
    public function getCarriereSouhait()
    {
        return $this->carriereSouhait;
    }

    /**
     * @param int $carriereSouhait
     */
    public function setCarriereSouhait($carriereSouhait)
    {
        $this->carriereSouhait = $carriereSouhait;
    }

    /**
     * @return string
     */
    public function getCarriereSouhaitcom()
    {
        return $this->carriereSouhaitcom;
    }

    /**
     * @param string $carriereSouhaitcom
     */
    public function setCarriereSouhaitcom($carriereSouhaitcom)
    {
        $this->carriereSouhaitcom = $carriereSouhaitcom;
    }

    /**
     * @return int
     */
    public function getEquipeClient()
    {
        return $this->equipeClient;
    }

    /**
     * @param int $equipeClient
     */
    public function setEquipeClient($equipeClient)
    {
        $this->equipeClient = $equipeClient;
    }

    /**
     * @return string
     */
    public function getEquipeClientcom()
    {
        return $this->equipeClientcom;
    }

    /**
     * @param string $equipeClientcom
     */
    public function setEquipeClientcom($equipeClientcom)
    {
        $this->equipeClientcom = $equipeClientcom;
    }

    /**
     * @return int
     */
    public function getChefprojet()
    {
        return $this->chefprojet;
    }

    /**
     * @param int $chefprojet
     */
    public function setChefprojet($chefprojet)
    {
        $this->chefprojet = $chefprojet;
    }

    /**
     * @return string
     */
    public function getChefprojetcom()
    {
        return $this->chefprojetcom;
    }

    /**
     * @param string $chefprojetcom
     */
    public function setChefprojetcom($chefprojetcom)
    {
        $this->chefprojetcom = $chefprojetcom;
    }

    /**
     * @return int
     */
    public function getIntTech()
    {
        return $this->intTech;
    }

    /**
     * @param int $intTech
     */
    public function setIntTech($intTech)
    {
        $this->intTech = $intTech;
    }

    /**
     * @return string
     */
    public function getIntTechcom()
    {
        return $this->intTechcom;
    }

    /**
     * @param string $intTechcom
     */
    public function setIntTechcom($intTechcom)
    {
        $this->intTechcom = $intTechcom;
    }

    /**
     * @return int
     */
    public function getIntFonct()
    {
        return $this->intFonct;
    }

    /**
     * @param int $intFonct
     */
    public function setIntFonct($intFonct)
    {
        $this->intFonct = $intFonct;
    }

    /**
     * @return string
     */
    public function getIntFonctcom()
    {
        return $this->intFonctcom;
    }

    /**
     * @param string $intFonctcom
     */
    public function setIntFonctcom($intFonctcom)
    {
        $this->intFonctcom = $intFonctcom;
    }

    /**
     * @return int
     */
    public function getSiteGeo()
    {
        return $this->siteGeo;
    }

    /**
     * @param int $siteGeo
     */
    public function setSiteGeo($siteGeo)
    {
        $this->siteGeo = $siteGeo;
    }

    /**
     * @return string
     */
    public function getSiteGeocom()
    {
        return $this->siteGeocom;
    }

    /**
     * @param string $siteGeocom
     */
    public function setSiteGeocom($siteGeocom)
    {
        $this->siteGeocom = $siteGeocom;
    }

    /**
     * @return int
     */
    public function getDeplHorair()
    {
        return $this->deplHorair;
    }

    /**
     * @param int $deplHorair
     */
    public function setDeplHorair($deplHorair)
    {
        $this->deplHorair = $deplHorair;
    }

    /**
     * @return string
     */
    public function getDeplHoraircom()
    {
        return $this->deplHoraircom;
    }

    /**
     * @param string $deplHoraircom
     */
    public function setDeplHoraircom($deplHoraircom)
    {
        $this->deplHoraircom = $deplHoraircom;
    }

    /**
     * @return int
     */
    public function getGlobalmi()
    {
        return $this->globalmi;
    }

    /**
     * @param int $globalmi
     */
    public function setGlobalmi($globalmi)
    {
        $this->globalmi = $globalmi;
    }

    /**
     * @return string
     */
    public function getGlobalmicom()
    {
        return $this->globalmicom;
    }

    /**
     * @param string $globalmicom
     */
    public function setGlobalmicom($globalmicom)
    {
        $this->globalmicom = $globalmicom;
    }

    /**
     * @return string
     */
    public function getEvolutionLast()
    {
        return $this->evolutionLast;
    }

    /**
     * @param string $evolutionLast
     */
    public function setEvolutionLast($evolutionLast)
    {
        $this->evolutionLast = $evolutionLast;
    }

    /**
     * @return string
     */
    public function getCommentsmi()
    {
        return $this->commentsmi;
    }

    /**
     * @param string $commentsmi
     */
    public function setCommentsmi($commentsmi)
    {
        $this->commentsmi = $commentsmi;
    }

    /**
     * @return int
     */
    public function getRelStructure()
    {
        return $this->relStructure;
    }

    /**
     * @param int $relStructure
     */
    public function setRelStructure($relStructure)
    {
        $this->relStructure = $relStructure;
    }

    /**
     * @return string
     */
    public function getRelStructurecom()
    {
        return $this->relStructurecom;
    }

    /**
     * @param string $relStructurecom
     */
    public function setRelStructurecom($relStructurecom)
    {
        $this->relStructurecom = $relStructurecom;
    }

    /**
     * @return int
     */
    public function getEcouteBesoin()
    {
        return $this->ecouteBesoin;
    }

    /**
     * @param int $ecouteBesoin
     */
    public function setEcouteBesoin($ecouteBesoin)
    {
        $this->ecouteBesoin = $ecouteBesoin;
    }

    /**
     * @return string
     */
    public function getEcouteBesoincom()
    {
        return $this->ecouteBesoincom;
    }

    /**
     * @param string $ecouteBesoincom
     */
    public function setEcouteBesoincom($ecouteBesoincom)
    {
        $this->ecouteBesoincom = $ecouteBesoincom;
    }

    /**
     * @return int
     */
    public function getStrategieDev()
    {
        return $this->strategieDev;
    }

    /**
     * @param int $strategieDev
     */
    public function setStrategieDev($strategieDev)
    {
        $this->strategieDev = $strategieDev;
    }

    /**
     * @return string
     */
    public function getStrategieDevcom()
    {
        return $this->strategieDevcom;
    }

    /**
     * @param string $strategieDevcom
     */
    public function setStrategieDevcom($strategieDevcom)
    {
        $this->strategieDevcom = $strategieDevcom;
    }

    /**
     * @return int
     */
    public function getPoliticSalarial()
    {
        return $this->politicSalarial;
    }

    /**
     * @param int $politicSalarial
     */
    public function setPoliticSalarial($politicSalarial)
    {
        $this->politicSalarial = $politicSalarial;
    }

    /**
     * @return string
     */
    public function getPoliticSalarialcom()
    {
        return $this->politicSalarialcom;
    }

    /**
     * @param string $politicSalarialcom
     */
    public function setPoliticSalarialcom($politicSalarialcom)
    {
        $this->politicSalarialcom = $politicSalarialcom;
    }

    /**
     * @return int
     */
    public function getEspritEnt()
    {
        return $this->espritEnt;
    }

    /**
     * @param int $espritEnt
     */
    public function setEspritEnt($espritEnt)
    {
        $this->espritEnt = $espritEnt;
    }

    /**
     * @return string
     */
    public function getEspritEntcom()
    {
        return $this->espritEntcom;
    }

    /**
     * @param string $espritEntcom
     */
    public function setEspritEntcom($espritEntcom)
    {
        $this->espritEntcom = $espritEntcom;
    }

    /**
     * @return int
     */
    public function getTransparence()
    {
        return $this->transparence;
    }

    /**
     * @param int $transparence
     */
    public function setTransparence($transparence)
    {
        $this->transparence = $transparence;
    }

    /**
     * @return string
     */
    public function getTransparencecom()
    {
        return $this->transparencecom;
    }

    /**
     * @param string $transparencecom
     */
    public function setTransparencecom($transparencecom)
    {
        $this->transparencecom = $transparencecom;
    }

    /**
     * @return int
     */
    public function getPoliticInfo()
    {
        return $this->politicInfo;
    }

    /**
     * @param int $politicInfo
     */
    public function setPoliticInfo($politicInfo)
    {
        $this->politicInfo = $politicInfo;
    }

    /**
     * @return string
     */
    public function getPoliticInfocom()
    {
        return $this->politicInfocom;
    }

    /**
     * @param string $politicInfocom
     */
    public function setPoliticInfocom($politicInfocom)
    {
        $this->politicInfocom = $politicInfocom;
    }

    /**
     * @return int
     */
    public function getGlobalap()
    {
        return $this->globalap;
    }

    /**
     * @param int $globalap
     */
    public function setGlobalap($globalap)
    {
        $this->globalap = $globalap;
    }

    /**
     * @return string
     */
    public function getGlobalapcom()
    {
        return $this->globalapcom;
    }

    /**
     * @param string $globalapcom
     */
    public function setGlobalapcom($globalapcom)
    {
        $this->globalapcom = $globalapcom;
    }

    /**
     * @return string
     */
    public function getCommentsap()
    {
        return $this->commentsap;
    }

    /**
     * @param string $commentsap
     */
    public function setCommentsap($commentsap)
    {
        $this->commentsap = $commentsap;
    }

}
