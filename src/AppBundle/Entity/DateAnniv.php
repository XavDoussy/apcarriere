<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * DateAnniv
 *
 * @ORM\Table(name="date_anniv")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\DateAnnivRepository")
 */
class DateAnniv
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var User
     *
     * @ORM\OneToOne(targetEntity="User", inversedBy="dateAnniv", cascade={"persist", "merge"})
     */
    private $user;

    /**
     * @var int
     *
     * @ORM\Column(name="status", type="integer", nullable=true)
     */
    private $status;

    /**
     * @return int
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param int $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param User $user
     */
    public function setUser($user)
    {
        $this->user = $user;
    }

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="nextSurvey", type="datetime", nullable=true)
     */
    private $nextSurvey;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nextSurvey
     *
     * @param \DateTime $nextSurvey
     *
     * @return DateAnniv
     */
    public function setNextSurvey($nextSurvey)
    {
        $this->nextSurvey = $nextSurvey;

        return $this;
    }

    /**
     * Get nextSurvey
     *
     * @return \DateTime
     */
    public function getNextSurvey()
    {
        return $this->nextSurvey;
    }

}
